#!/usr/bin/python

import sys, os, shutil

MOD_DIR='__'
shutil.rmtree(os.path.join(sys.argv[1], MOD_DIR))
os.makedirs(os.path.join(sys.argv[1], MOD_DIR))

for filename in os.listdir(sys.argv[1]):
    if not filename.endswith('.txt') or filename.startswith('__'):
        continue

    f=open(os.path.join(sys.argv[1],filename), 'r')
    ll=f.readlines()

    ff=open(os.path.join(sys.argv[1], MOD_DIR, filename), 'w')
    for line in ll:
        if line.startswith('\\beginsong{'):
            line = line.replace('}', '{').split('{')
            ff.write('\\beginsong{')
            print line[1].lower().capitalize()
            ff.write(line[1].lower().capitalize())
            ff.write('}')
            ff.write(line[2])
            print line[2]
        else:
            ff.write(line)
    ff.close()
    f.close()
