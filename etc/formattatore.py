#!/usr/bin/python

import sys, os

for filename in os.listdir(sys.argv[1]):
    if not filename.endswith('.txt') or filename.startswith('__'):
        continue

    f=open(os.path.join(sys.argv[1],filename), 'r')
    ll=f.readlines()

    ff=open(os.path.join(sys.argv[1],'__'+filename), 'w')
    for line in ll:
        linesplit = line.split('   ')
        newline = False

        for token in linesplit:
            if token is '':
                newline = True
            else:
                if newline is True:
                    ff.write('\n')
                if not token == '\n' and not token == '\r\n':
                    token = token.lstrip()
                ff.write(token)

    ff.close()
    f.close
