@84 DIO DELLA GENTE

Cristo Signore vivo tra la gente tu che fai strada
sempre in mezzo a noi, tu parli al cuore e ci fai 
capire quanto � felice chi sta insieme a te.
E camminando spieghi la parola e chi � deluso pace 
trover�, tu sei l'amico che ti fai amico per l'uomo 
d'oggi che ti cercher�.

Resta con noi Signore qui con noi, quando verr� la 
sera ci sarai, ma se ci manchi tu il buio ci sar�,
spezza quel pane ancora insieme a noi, ma se ci 
manchi tu il buio ci sar�, spezza quel pane ancora 
insieme a noi.

Tu nella chiesa crei la famiglia la comunione di tutti i 
figli tuoi, unica mensa unico perdono centro vitale 
per l'umanit�. Annunceremo ovunque tu ci chiami che 
noi ci amiamo nel nome tuo Ges�, siamo profeti solo 
di un amore che pagheremo nella verit�.
Resta con noi signore..

Chiesa di Roma � Pietro che ti guida, forza di pace a 
tutti donerai, terzo millennio di vangelo vivo
testimonianza nella carit�.
E sul cammino veglia tu Maria, madre e signora sei 
della citt�, Cristo risorto Dio della gente alla tua porta 
oggi busser�. Resta con noi Signore..



@85 LAUDATO SII

Laudato sii, o mi Signore,
Laudato sii, o mi Signore,
Laudato sii, o mi Signore,
Laudato sii, o mi Signore.


E per tutte le creature per il sole e per la luna per le 
stelle e per il vento e per l'acqua e per il fuoco.
Laudato sii..

Per sorella madre terra ci alimenta e ci sostiene, per 
i frutti, i fiori e l'erba per i monti e per il mare.
Laudato sii..
 
Perch� il senso della vita � cantare e 
lodarti e perch� la nostra vita sia sempre una canzone.
Laudato sii..

E per quelli che ora piangono e per quelli che ora
soffrono e per quelli che ora nascono e per quelli che
ora muoiono. Laudato sii..



@86 DIMMI CHIARA

Dimmi Chiara, quanti anni hai? Sei giovane e molte
cose imparerai, vedi Chiara non vedrai pi� i tuoi
amici, ma vivr� giorni felici, di chiudere i miei
vent'anni non ho paura dietro una grata e quattro
mura, perch� li palpita il cuore del mio Dio e a lui
canter� gloria e onore. Ci� che vedo adesso non lo
vedr� pi� ma vedr� il mio Signore ogni giorno di pi�
perch� li dentro sar� il mondo intero, ci sar� l'amore
vero. 
Dimmi Chiara, quanti anni hai? Sei giovane e molte
cose imparerai, vedi chiara come splende sul tuo viso
quella luce, luce del paradiso; taglier� questi lunghi
capelli dimenticher� i miei giorni brutti e belli,
svuoter� ogni cosa che si trova dentro me perch� il
Signore vuole tutto per se, vivr� cos� con la sua
povert� vivr� in letizia e con semplicit�, ringrazier�
sempre ogni giorno il mio Signore perch� mi dar� il
suo cuore.
Senti Chiara, sei sempre giovane e da te molte cose
imparer�, stammi vicino in questo mio canto, io sar�
con te!



@87 CHIARA E FRANCESCO

Chiara, sorella mia, tu hai visto Dio era lungo una
strada, ti sei innamorata, ora ti condurr� con la sua
mano dove lui vorr�.

Francesco, fratello mio, in te ho visto Dio era lungo
una strada, mi sono innamorata, ora mi condurr� con 
la sua mano dove lui vorr�.

Vieni, corriamo insieme dove ci porta Dio, egli lungo
le strade ci guida da innamorati, ora ci condurr� con 
la sua mano dove lui vorr�.



@88 DOLCE SENTIRE

Dolce � sentire come nel mio cuore ora umilmente 
sta nascendo amore, dolce � capire che non son pi� solo,
ma che son parte di una immensa vita, che generosa 
risplende intorno a me, dono di Lui, del suo immenso amore.

Ci ha dato il cielo e le chiare stelle fratello sole e 
sorella luna, la madre terra con frutti prati e fiori,
il fuoco e il vento, l�aria e l�acqua pura fonte di vita 
per le sue creature, dono di Lui del suo immenso 
amore, dono di lui del suo immenso amore.

Sia laudato nostro Signore, che ha creato l�universo 
intero, sia laudato nostro Signore, noi tutti siamo sue
creature, dono di lui, del suo immenso amore, dono di
lui del suo immenso amore.



@89 ECCOMI

Ora tu verrai da me Signore sazierai la fame del mio
cuore, non sar� pi� io che vivr�, ma tu che vivrai
dentro me; dolcemente tu mi cercherai
dolcemente ti risponder�, un sussurro la tua volont�,
una gioia che mi inonder�.

Eccomi, eccomi, tu la mia speranza, tu la verit�,
eccomi, eccomi tu il mio conforto, tu la libert�.

La tua pace adesso � nel mio cuor la tua gioia scorre
dentro me, un'amore infinito che il mio mondo tutto
invade gi�; e per mano tu mi prenderai luce per i 
passi miei sarai, o Signore, per te canter� perch� so
che non mi lascerai. Eccomi, eccomi..



@90 ECCOMI, SIGNORE IO VENGO

Eccomi, eccomi, Signore, io vengo, eccomi, eccomi, si 
compia in me la tua volont�.

Nel mio Signore ho sperato e su di me s'� chinato, ha 
dato ascolto al mio grido m'ha liberato dalla morte.
Eccomi..

I miei piedi ha reso saldi sicuri ha reso i miei passi,
ha messo sulla mia bocca un nuovo canto di lode.
Eccomi..

Il sacrificio non gradisci ma m'hai aperto l'orecchio,
non hai voluto olocausti, allora ho detto: �io vengo!�.
Eccomi..

Sul tuo libro di me � scritto: �Si compia il tuo volere�.
Questo mio Dio desidero, la tua legge � nel mio 
cuore. Eccomi..

La tua giustizia ho proclamato, non tengo chiuse le 
labbra, non rifiutarmi, Signore, la tua misericordia.
Eccomi..



@91 PANE DEL CIELO

Pane del cielo, sei Tu Ges�,
via d�amore tu ci fai come Te.

No, non � rimasta fredda la terra, tu sei rimasto con 
noi per nutrirci di Te, pane di vita ed infiammare col 
tuo amore tutta l�umanit�. Pane del cielo..

S�, il cielo � qui su questa terra, tu sei rimasto 
con noi ma ci porti con Te nella tua casa dove 
vivremo insieme a Te tutta l�eternit�.
Pane del cielo..

No, la morte non pu� farci paura, tu sei rimasto 
con noi e chi vive di Te vive per sempre sei Dio con 
noi sei Dio per noi, Dio in mezzo a noi.
Pane del cielo..



@92 E' BELLO

� bello andar coi miei fratelli per le vie del mondo e 
poi scoprire te nascosto in ogni cuor; e vedere che 
ogni mattino tu ci fai rinascere e fino a sera sei 
vicino nella gioia e nel dolor.

Grazie perch� sei con me grazie perch� se ci amiamo
rimani tra noi.

� bello udire la tua voce che ci parla delle grandi 
cose fatte dalla tua bont�; vedere l�uomo fatto a 
immagine della tua vita fatto per conoscere in te il 
mistero della Trinit�. Grazie perch�..

� bello dare questa lode a te portando a tutto il 
mondo il nome tuo Signor che sei l�amor; uscire e per 
le vie cantare che abbiamo un Padre solo e tutti 
quanti siamo figli veri nati dal Signor. Grazie perch�..



@93 E CORREREMO INSIEME

Davvero questa messa non finisce qui il sole non 
muore la sera, la festa con gli amici non finisce con 
un ciao e torni poi a casa contento.

E correremo insieme la vita nel sole la gioia e il dolore
di tutto il creato e non avremo niente te solo 
Signore un amico per tutta la gente del mondo.

Ti dico ancora grazie per il pane che ho mangiato a 
casa ritrovo quel pane, la gioia dei fratelli che ho 
incontrato in te rivive se amo ogni uomo.
E correremo insieme..

Incontrer� bambini che giocano anche tristi una 
madre che � in ansia ma vive, degli uomini gi� stanchi
ma che in fondo son felici � questa la chiesa pi� vera.
E correremo insieme..

Ti prego padre grande per i poveri del mondo per 
quelli che io ho creato, la vita � una stagione che fa 
nascere  dei fiori ciascuno vale il tuo amore. 
E correremo insieme..



@94 GRANDI COSE

Grandi cose ha fatto il Signore per noi, ha fatto 
germogliare fiori fra le rocce, grandi cose ha fatto
il Signore per noi, ci ha riportati liberi alla nostra 
terra; ed ora possiamo cantare possiamo gridare
l'amore che Dio ha versato su noi.

Tu che sai strappare dalla morte hai sollevato il 
nostro viso dalla polvere tu che hai sentito il 
nostro pianto, nel nostro cuore hai messo un seme di 
felicit�. Grandi cose..



@95 EMMANUEL

Dall'orizzonte una grande luce viaggia nella storia e 
lungo gli anni ha vinto il buio facendosi memoria e 
illuminando la nostra vita chiaro ci rivela che non si 
vive se non si cerca la verit�.
Da mille strade arriviamo a Roma sui passi della fede
sentiamo l'eco della parola che risuona ancora da 
queste mura da questo cielo per il mondo intero � 
vivo oggi � l'uomo vero, Cristo Ges�.

Siamo qui sotto la stessa luce sotto la sua croce
cantando ad una voce: � l'Emmanuel, l'Emmanuel, 
l'Emmanuel.
 
Un grande dono che Dio c'ha fatto � Cristo il suo 
Figlio l'umanit� � rinnovata � in lui salvata, � vero 
uomo, � vero Dio � il Pane della Vita che ad ogni uomo
ai suoi fratelli ridoner�. Siamo qui..

La morte � uccisa la vita ha vinto, � Pasqua in tutto 
il mondo un vento soffia in ogni uomo lo Spirito fecondo,
che porta avanti nella storia la Chiesa sua sposa sotto 
lo sguardo di Maria, comunit�. Siamo qui..
 
E' giunta un'era di primavera � tempo di cambiare � 
oggi il tempo sempre nuovo per ricominciare per 
nuove svolte parole nuove e convertire il cuore per dire 
al mondo e ad ogni uomo Signore Ges�.
Siamo qui.. 


@96 TE AL CENTRO DEL MIO CUORE

Ho bisogno di incontrarti nel mio cuore di trovare te 
di stare insieme a te unico riferimento del mio 
andare unica ragione tu, unico sostegno tu, al centro 
del mio cuore ci sei solo Tu.

Anche il cielo gira intorno e non ha pace ma c�� un 
punto fermo � quella stella l� la stella polare � fissa ed 
� la sola la stella polare tu, la stella sicura tu, al 
centro del mio cuore ci sei solo tu.

Tutto ruota intorno a Te, in funzione di te e poi non 
importa il come il dove e il se.

Che Tu splenda sempre al centro del mio cuore il 
significato allora sarai tu quello che far� sar� 
soltanto amore unico sostegno tu, la stella polare tu
al centro del mio cuore ci sei solo tu.
Tutto ruota intorno a te..


@97 GESU' UN AMICO

Se hai trovato un amico tu sei il pi� ricco del mondo
e ormai la notte mai pi� scender� per questo puoi
cantar:

Amo la vita perch� posso amare ogni giorno, amo la
vita perch� amo tutti il Signore � con me.

Quando stai insieme agli amici ne puoi contare uno in
pi� anche se non lo conosci lui c'� ha un volto, � Ges�.
Amo la vita..

E' il volto della mamma, � il volto del tuo pap�, �
il volto vero dell'umanit� che cerca libert�.
Amo la vita..

Aveva anche lui degli amici eran simpatici e no non
diede loro dei soldi o l'onor, don� tutto il suo amor.
Amo la vita..

Venne un amico di notte viveva ancora con lui gli
disse "amico" ma poi lo trad� l'amore non fin�.
Amo la vita..

E' tutto ci� che di eterno resta tra gli uomini ancora
la vita vale se tu sai amor e l'oggi � eternit�.
Amo la vita..



@98 E SONO SOLO UN UOMO

Io lo so Signore che vengo da lontano prima nel 
pensiero e poi nella tua mano io mi rendo conto che 
tu sei la mia vita e non mi sembra vero di pregarti 
cos�.
Padre d'ogni uomo e non ti ho visto mai, spirito di vita e 
nacqui da una donna, figlio mio fratello e sono solo un 
uomo eppure io capisco che tu sei verit�.

E imparer� a guardare tutto il mondo con gli occhi 
trasparenti di un bambino e insegner� a chiamarti 
"Padre nostro" ad ogni figlio che diventa uomo. (2 volte)

Io lo so Signore che tu mi sei vicino luce alla mia 
mente guida al mio cammino, mano che sorregge
 sguardo che perdona e non mi sembra vero che tu 
esista cos�.
Dove nasce amore Tu sei la sorgente dove c'� una 
croce tu sei la speranza dove il tempo ha fine tu sei 
la vita eterna e so che posso sempre contare su di 
te.

E accoglier� la vita come un dono e avr� il coraggio di 
morire anch'io e incontro a te verr� col mio fratello
che non si sente amato da nessuno. (2 volte)



@99 E' PACE INTIMA

Le ore volano via il tempo s'avvicina lungo la strada 
canto a te, nella tua casa so che ti incontrer� e 
sar� una festa trovarti ancora.

E' pace intima la tua presenza qui mistero che non so
spiegarmi mai, � cielo limpido � gioia pura che mi fa 
conoscere chi sei per me .

Sembra impossibile ormai pensare ad altre cose, non posso 
fare a meno di te, sembrano eterni 
gli attimi che non ci sei ed aspetto solo di ritrovarti.
E' pace..

E' la pi� bella poesia dirti il mio s� per sempre e nel 
segreto parlare con te, semplici cose parole che tu 
sai, note del mio canto nel tuo silenzio. E' pace..



@100 IL TUO AMORE

Grande � la pace dentro me chiara � la luce che mi
da occhi per guardare al di l� per vedere quelle che
sta nascendo dentro me, alzer� le mie mani verso te
lascer� il passato dietro me seguir� la tua strada la
tua voce perch� adesso tutto tace, ora ascolto solo
te.

Come sorgente di acqua chiara fuoco che mai si
consuma come vento che soffia e che tutto
travolger� perch� � cos� che tu vieni che salvi il tuo
popolo tu Signore pastore dell'anima, tu che parli al
mio cuore che ha sete soltanto del tuo amore.

Grande � la pace intorno a te chiara � la luce che ti
da occhi per guardare al di l� per vedere quello che
sta nascendo dentro te, alza le tue mani verso me
lascia il passato dietro te segui
la mi strada la mia voce perch� adesso tutto tace,
e tu ascolti solo me.

Come sorgente di acqua chiara fuoco che mai si
consuma come vento che soffia e che tutto
travolger� perch� � cos� che io vengo che salvo il mio
popolo io che sono il pastore dell'anima, io che parlo
al tuo cuore che ha sete soltanto del mio amore.



@101 IL MONDO INTORNO A ME

Tante cose da fare tante inquietudini da calmare
tante notti di grilli e stelle tante albe da spettare.
Tanto amore da fare tante strade da camminare
tante catene da spezzare tanti sogni per volare.
Tante speranze da non soffocare fra la gioia poter
cantare tante lacrime da asciugare e bambini da far
giocare.
Tanti vecchi da accarezzare mani e cuori da
innamorare tanti si da rinverdire tanto buio da
illuminare.
Tante montagne da salire tanti fiori da respirare
tanta terra da coltivare tanti semi da far sbocciare.
Spicchi di luna da contemplare tanto sole per
riscaldare onde di mare da scivolare la mia vita.. da regalare.



@102 IL SIGNORE E' BUONO E GIUSTO

Nella casa del Signore puoi trovare se lo vuoi quel 
che il mondo non potr� donarti mai: la fiducia in lui,
la speranza in Lui e la gioia di sentirsi amati sempre 
da lui.

Il Signore � buono e giusto sa capire e perdonare,
vieni, fratello, vieni all'altare del Signor.

L'importante � non agire con l'inganno e la menzogna
ma cercare con la fede e l'umilt�: la fiducia in lui, la 
speranza in lui e la gioia di sentirsi amati sempre 
da lui. Il Signore �..

E' la casa del Signore il rifugio e la fortezza di chi � 
solo e ha bisogno di sentir: la fiducia in lui, la 
speranza in lui e la gioia di sentirsi amati sempre da 
lui. Il Signore �..



@103 IL REGNO DI DIO

Il regno di Dio � qui in mezzo a noi, il regno di Dio
viene in umilt� beato chi l'accoglie in semplicit�.

A cosa � simile il regno del Signore, � simile a un 
granello di senapa, un uomo lo ha seminato nel suo 
campo ed ora ha prodotto i suoi frutti. Il regno..

A cosa � simile il regno del Signore, � simile a un 
poco di lievito, � stato nascosto in tre staia di farina,
perch� tutta la pasta fermenti. Il regno..

Il regno dei cieli � dei poveri e dei miti per quelli che 
han saputo accoglierlo, � fatto per chi � testimone 
dell�amore ed � perseguitato dagli uomini.
Il regno..



@104 IL SANTO VIAGGIO

Beato chi decide nel suo cuore e il santo viaggio e 
s'incammina per le vie della vita del Signore. Beato
chi ripone in Dio la forza, il suo cuore, il suo domani
perch� Dio a lui provveder�.

Dio tu mi dai ali di colomba per volare verso il sole,
Dio tu mi dai piedi di cervo per camminare sulle alte
vette.

Voglio cantare, cantare al Signore, al mio Dio finch�
ho vita riposare nel suo immenso amore.
A lui sia gradito il mio canto perch� la mia gioia
� nel Signore, solo in lui. Dio tu mi dai..



@105 IL PANE DEL CAMMINO

Il tuo popolo in cammino cerca in te la guida sulla 
strada verso il Regno sei sostegno col tuo corpo,
resta sempre con noi, o Signore.

� il tuo pane Ges� che ci d� forza e rende 
pi� sicuro il nostro passo se il vigore nel 
cammino si svilisce la tua mano dona lieta la 
speranza. Il tuo popolo..

� il tuo vino Ges� che ci disseta e sveglia in noi 
l�ardore di seguirti se la gioia cede il passo alla 
stanchezza, la tua voce fa rinascere 
freschezza. Il tuo popolo..

� il tuo corpo Ges� che ci fa chiesa fratelli sulle 
strade della vita, se il rancore toglie luce 
all�amicizia dal tuo cuore nasce giovane 
il perdono. Il tuo popolo..



@106 IO SARO' CON TE

Io sar� con te accompagner� i tuoi passi strade di
speranza nel deserto aprir� non sarai mai solo sulle
strade della vita, va e non temere acqua viva ti dar�.

Ti sei fatto per me pane, mi hai donato la libert�, tu
mi chiami a camminare sulle strade dell'amore, ma
son deboli i miei passi per seguirti mio Signore, sono
giovane ho paura dentro al cuore. Io sar� con te..

Tu mi avvolgi col tuo cuore e mi doni la tua vita, tu mi
inviti a partire e con gioia te servire, ma son deboli i
miei passi per seguirti mio Signore, sono giovane ho 
paura dentro al cuore. Io sar� con te..

Tu mi guardi nel profondo e conosci il mio cuore tu
mi chiami ad esser servo, testimone del tuo amore, ma
son deboli i miei passi per seguirti mio Signore, sono
giovane ho paura dentro al cuore. Io sar� con te..



@107 L'UOMO NUOVO

Dammi un cuore Signor grande per amar, dammi un 
cuore Signor, pronto a lottare con te.

L'uomo nuovo creatore della storia costruttore di 
nuova umanit�, l'uomo nuovo che vive l'esistenza come
un rischio che il mondo cambier�. Dammi un cuore..

L'uomo nuovo che lotta con speranza nella vita cerca
verit�, l'uomo nuovo non stretto da catene, l'uomo
libero che esige libert�. Dammi un cuore..

L'uomo nuovo che pi� non vuoi frontiere n� violenze
in questa societ�, l'uomo nuovo al fianco di chi
soffre, dividendo con lui il tetto e il pane. Dammi un cuore..



@108 MAGNIFICAT

Dio ha fatto in me cose grandi, lui che guarda l'umile
serva e disprezza i superbi dall'orgoglio del cuore.

L'anima mia esulta in Dio mio Salvatore
l'anima mia esulta in Dio mio Salvatore, la sua 
salvezza canter�.

Lui onnipotente e santo, lui abbatte i grandi dai troni
e solleva dal fango il suo umile servo. L'anima mia..

Lui amore sempre fedele, lui rid� al suo servo il suo
amore e ricorda il suo patto stabilito per sempre.
L'anima mia..



@109 NELLA TUA TENDA

Nella tua tenda Signore con te fammi restare 
perch� ora ho capito che un posto non c'� ch'� pi� 
sicuro per me voglio servirti e voglio amarti con 
tutto il cuore per sempre.

Nella tua tenda fammi restare,
sar� sicuro, l� ci sei Tu.

Alle tue mani mi affido Signor, la mia salvezza sei tu
e della roccia pi� forte sar� se accanto a te rester�
voglio servirti e voglio amarti con tutto il cuore per 
sempre. Nella tua tenda..
          
Tu che sei tutto il mio mondo quaggi�, no non 
lasciarmi mai pi�, guida i miei passi cos� non cadr�
sulle tue strade, Signor voglio servirti e voglio amarti
con tutto il cuore per sempre. Nella tua tenda..




@110 NUOVA ARMONIA

Finalmente davanti a te ora sono Signore, i miei
occhi perduti nei tuoi che stan parlando d'amore.
Sento quasi una nuova armonia mentre mi chiami per
nome come un canto nell'anima mia una nuova
emozione.

E piano piano infondo al cuore sento che nasce una
sorgente d'amore e come un fiume la dove scorre
porta la vita, poi tra l'erba si confonde.
Finalmente una cosa sola io e te mio Signore, il mio
cuore che � fuso nel tuo in un unico amore.

E la mia mente nel tuo pensiero come un gabbiano sta
volando gi� ed il tuo spirito � come un vento leggero
mi porta su ali di libert�, e nella mia mente in tutto
mio cuore ora tu vivi Signore.

Una nuova armonia - ora tu vivi in me
ora tu vivi in me - una nuova armonia.
Signore.




@111 ORA CHE IL GIORNO FINISCE

Dio quante volte ho pensato la sera di non averti
incontrato per niente e la memoria del canto di ieri
come d'un tratto sembrava lontana.
Dio quante volte ho abbassato lo sguardo spento il
sorriso nascosta la mano, quante parole lasciate
cadere, quanti silenzi ti chiedo perdono.

Io ti ringrazio per ogni creatura per ogni momento
del tempo che vivo, io ti ringrazio perch� questo
canto libero e lieto ti posso cantare. (2 volte)

Ora che il giorno finisce Signore ti voglio cantare
parole d'amore, voglio cantare la gente incontrata il
tempo vissuto le cose che ho avuto.
Sorrisi di gioia, parole scambiate, le mani intrecciate
nel gesto di pace e dentro le cose pensiero
improvviso la tua tenerezza il tuo stesso sorriso.
Io ti ringrazio..




@112 ORA SIAMO ARRIVATI

Ora siamo arrivati nella tua casa per ascoltare la tua
parola per acclamare la tua grandezza cantiamo a te,
alleluia, Padre che i nostri cuori tu sai unire,
insegnaci che cos'� l'amore siamo riuniti intorno a te.

Tu sei venuto in mezzo a noi, fonte della vita,
speranza di salvezza, infondi gioia nell'anima e poi
trasformi l'odio in amore per noi, tutto il creato
canta: alleluia! Ora siamo arrivati..

Tu sei sorgente di purezza infinita carit�, salva tutti
noi, come acqua viva ci rinnoverai,
con il tuo verbo poi ci sazierai, gloria a te, alleluia!
Ora siamo arrivati..

Tu sei la nostra guida meta senza fine che seguiremo
con fedelt�, di mille voci una sola sar�, che come un
grido a te giunger�, gloria all'altissimo, alleluia!
Ora siamo arrivati..




@113 PRENDEREMO IL LARGO

Questo � il nostro tempo per osare per andare, la
parola che ti chiama � quella tua, come un giorno a 
Pietro anche oggi dici a noi: "getta al largo le tue
reti insieme a me!"

Saliremo in questa barca anche noi il tuo vento
soffia gi� sulle vele, prenderemo il largo dove vuoi tu
navigando insieme a te Ges�.

Questo � il nostro tempo questo � il mondo che ci
dai, orizzonti e nuove vie d'umanit�, come un giorno
a Pietro anche oggi dici a noi: "se ami pi� di tutto
segui me!". Saliremo..

Navigando il mare della storia insieme a te la tua
barca in mezzo a farti venti v�, come un giorno
a Pietro anche oggi dici a noi: "se tu credi in me tu
non affonderai!". Saliremo..




@114 ORA CHE SEI IN MEZZO A NOI

Veniamo a te Signore che sei tra di noi la verit� che
si diffonde in ogni cuore e tu spezzi il pane per noi,
pane vivo tu sei per nutrirci di immenso amore che
mai finir�.

Ora che sei in mezzo a noi insieme a noi tu resterai
cammineremo con te finch� vuoi e ovunque andrai, tu
che ci hai dato la vita donaci la gioia di vivere, tu
sei scintilla che accende il sole nell'oscurit�.

Grande Ges� che sei disceso per noi, col sacrificio 
salvasti il mondo dall'oscurit�, luce viva tu sei e ci
disseterai, attingeremo alla sorgente pura e limpida.

Sei ritornato tra noi insieme a noi tu resterai, uniti
insieme a te, non ci arrenderemo mai, ma tu che ci hai
dato la vita poi donaci la forza di vivere nel bene
e nella gioia immensa dell'umanit�.

Verso di te cibo di vita per noi, uniti a te nella tua
chiesa che diventer� fulcro di carit� perch� tu sei
libert�, ogni attimo ogni giorno per l'eternit�.

Ora che sei in mezzo a noi..
Sei ritornato tra noi..




@115 PADRE NOSTRO ASCOLTACI

Padre nostro ascoltaci con il cuore ti preghiamo
resta sempre accanto a noi, confidiamo in te, la tua
mano stendi sopra tutti i figli tuoi, il tuo regno venga
in mezzo a noi, il tuo regno venga in mezzo a noi.

Per il pane di ogni d� per chi vive per chi muore per
chi piange in mezzo a noi, noi preghiamo in te, per chi ha
il cuore vuoto per chi ormai non spera pi�, per chi
amore non ha visto mai, per chi amore non ha visto mai.

Se nel nome di Ges� con amore perdoniamo anche tu
che sei l'amore ci perdonerai, la tristezza dentro il
cuore non ritorner�, nel tuo nome gioia ognuno avr�,
nel tuo nome gioia ognuno avr�.




@116 RESTA QUI CON NOI

Le ombre si distendono scende ormai la sera e si
allontanano dietro i monti i riflessi di un giorno che
non finir� di un giorno che ora correr�
sempre, perch� sappiamo che una nuova vita da qui �
partita e mai pi� si fermer�.

Resta qui con noi il sole scende gi�, resta qui con noi,
Signore � sera ormai, resta qui con noi il sole scende
gia, se tu sei fra noi la notte non verr�.

S'allarga verso il mare il tuo cerchio d'onda
che il vento spinger� fino a quando giunger�
ai confini di ogni cuore alle porte dell'amore vero,
come una fiamma che dove passa brucia cos� il tuo
amore tutto il mondo invader�. Resta qui con noi..

Davanti a noi l'umanit� lotta soffre e spera come una
terra che nell'arsura chiede l'acqua da un cielo senza
nuvole ma che sempre le pu� dare vita, con te
saremo sorgente d'acqua pura con te fra noi il
deserto fiorir�. Resta qui con noi..




@117 PADRE NOSTRO

Padre nostro che sei nei cieli sia santificato il tuo nome
tuo, venga il tuo regno sia fatta la tua volont� come
in cielo e cos� in terra, come il cielo e cos� in terra,
dacci oggi il nostro pane, dacci oggi il nostro pane
quotidiano, rimetti a noi i nostri debiti come noi li
rimettiamo ai nostri debitori e non ci indurre in t
tentazione ma liberaci dal male, e non ci indurre in
tentazione ma liberaci dal male.




@118 PRIMAVERA NELLA CHIESA

Le stelle nel cielo sono tante nel buio son come i figli
tuoi che vanno nella notte e forse non si vede ma �
gi� la prima luce di alberi inespressi che accendono
speranze.

E' ora che nasca la speranza in mezzo a noi, � ora
che nasca la tua gioia in mezzo a noi, � ora che nasce
il tuo amore in mezzo a noi, un mondo pi� vero
Signor.

Nei prati pi� nascosti son nati fiori a gruppi, non
cercano il rumore ma investono il tuo amore, dall'
albero maturo gi� cadon tanti fiori sembrava restar
solo ma presto dar� frutti, E' ora che nasca..

Abbiamo tante cose ci dai una cosa sola, il posto c'�
per tutti c'� il pane e c'� l'amore, ci chiami a stare 
insieme per vivere il vangelo e non aver paura se il
mondo non ci ama. E' ora che nasca..




@119 QUANTI PENSIERI

Signore tu mi conosci e sai tutto quello che so;
quando mi siedo, quando mi alzo, quando cammino,
quando riposo, ogni parola che sto per dire lo sai.

Quanti pensieri mio Dio hai per me se li canto son pi�
della sabbia, se li credo finiti mi accorgo che con te
con te sono ancora.

Signore tu mi circondi e ovunque io vada tu sei: sul
mare azzurro, sulle montagne, se sto da solo o in
compagnia, sono sicuro che tu stia sempre con me.
Quanti pensieri mio Dio..

Signore, non ero nato ed ero gi� vivo per te: ero
presente nella tua mente ogni mia forma, c
chi sarei stato, quel che avrei fatto tu lo sapevi gi�.
Quanti pensieri mio Dio..




@132 RISPOSTA

Quante le strade che un uomo far� e quando
fermarsi potr�? Quanti mari un gabbiano
dovr� attraversare per giungere e per riposar?
Quando tutta la gente del mondo riavr� per sempre
la sua libert�..

Risposta non c'� o forse chi lo sa 
caduta nel vento sar�. (2 volte)

Quando dal mare un'onda verr� che i monti
lavare potr�? Quante volte un uomo dovr� litigar
sapendo che � inutile odiar? E poi quante
persone dovranno morir perch� siano troppe a
morir. Risposta non c'�..

Quanti cannoni dovranno sparar e quando la
pace verr�? Quanti bimbi innocenti dovranno
morir e senza saperne il perch�? Quanto
giovane sangue versato sar� finch� un'alba
nuova verr�? Risposta non c'�..





@ NUOVA CAMMINERO'

Mi hai chiamato dal nulla, Signore e mi hai
dato il dono della vita. Tu mi hai preso e mi
hai messo per strada e mi hai detto di camminar.
Verso un mondo che non ha confini, no, verso
mete da raggiungere, oramai..
Verso il regno dell'amore che � sempre un p� 
pi� in l�.

Camminer� senza stancarmi e voler� sui
monti pi� alti e trover� la forza di andare
sempre pi� avanti.
S�, io camminer�, camminer�, con te vicino
io non cadr�, e camminer�, camminer�..

In ogni istante ti sento vicino, tu dai senso
alle cose che faccio. La tua luce mi indica
la strada e mi invita a camminar..
Verso un mondo che non ha confini, no, verso
mete da raggiungere, oramai..
Verso il regno dell'amore che � sempre un p�
pi� in l�. Camminer�..



















