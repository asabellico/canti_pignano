#!/bin/bash

pushd src
pdflatex -output-directory ../tmp accordi_chitarra.tex
pdflatex -output-directory ../tmp accordi_piano.tex
pdflatex -output-directory ../tmp testi.tex

## generate idxs
pushd ../tmp
texlua ../src/songidx.lua cbtitle.sxd cbtitle.sbx
texlua ../src/songidx.lua cbauth.sxd cbauth.sbx
texlua ../src/songidx.lua -b ../src/bible.can cbscript.sxd cbscript.sbx

texlua ../src/songidx.lua cballeluia.sxd cballeluia.sbx
popd

pdflatex -output-directory ../tmp accordi_chitarra.tex
pdflatex -output-directory ../tmp accordi_piano.tex
pdflatex -output-directory ../tmp testi.tex

pdflatex -output-directory ../tmp testi_a5.tex

mv ../tmp/accordi_chitarra.pdf ..
mv ../tmp/accordi_piano.pdf ..
mv ../tmp/testi.pdf ..
mv ../tmp/testi_a5.pdf ..
popd

zip -r canti_pignano.zip *.pdf
